# Repo for testing gitlab-ci between different branches 

```bash
git commit on develop
git push origin develop
# -> Here we recognized that all are ok
git checkout main
git merge develop --no-ff
git tag v0.18
git push origin v0.18
```
